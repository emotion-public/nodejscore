import * as express from "express";
import http from "http";
import cors from "cors";
import { JWT } from "./Middleware/Jwt";
import { ApiErrorHander } from "./Middleware/ApiErrorHandler";
import delay from "delay";
import LogMetrics from "./MetricsLogger";
import getUniqAppId from "../Utils/ApplicationID";

export default async function createServer({
  port,
  logEnabled = false,
  logService,
  cloudWatchLogConfig,
  applyApolloMiddleware,
  applyRoutes,
}: {
  port: string;

  logEnabled?: boolean;
  logService?: "cloudwatch";
  cloudWatchLogConfig?: {
    region: string;
    namespace: string;
  };

  applyApolloMiddleware: (arg: any) => Promise<any>;
  applyRoutes?: (arg: { app: express.Express }) => any;
}) {
  const app = express.default();
  app.use(express.json({ limit: "200mb" }));
  app.use(express.urlencoded({ limit: "200mb", extended: true }));
  app.use(cors());

  app.get("/healthz", async (req, res) => {
    res.json({ working: true });
  });

  app.get("/wait100ms", async (req, res) => {
    await delay(500);
    res.json({ working: true });
  });

  app.get("/printheader", async (req, res) => {
    res.json(req.headers);
  });

  await applyApolloMiddleware({
    app,
    logConfig: {
      logEnabled,
      logService,
      cloudWatchLogConfig,
    },
  });
  app.use(JWT);

  if (applyRoutes) {
    applyRoutes({ app });
  }

  /// ERROR HANDLER
  app.use(ApiErrorHander);

  app.use((err: any, req: any, res: any, _: any) => {
    // eslint-disable-next-line
    console.error(err);
    res.status(500);
    res.json({ error: "Unknwow" });
  });

  const server = http.createServer(app);
  // server.maxConnections = 100;
  if (logEnabled) {
    LogMetrics(server, {
      logService,
      cloudWatchConfig: cloudWatchLogConfig,
      uniqAppId: getUniqAppId(),
    });
  }
  server.listen(port, () => {
    // eslint-disable-next-line
    console.log("Server ready", port);
  });
}
