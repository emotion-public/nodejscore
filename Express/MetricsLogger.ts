import schedule from "node-schedule";
import http from "http";
import { sendLogNumber, OPTIONS } from "../Logger";

function logConnections(server: http.Server, options: OPTIONS) {
  let firstLogMade = false;
  schedule.scheduleJob("* * * * * *", () => {
    server.getConnections((error, count) => {
      if (!firstLogMade) {
        firstLogMade = true;
      }

      sendLogNumber("HTTPConnections", count, options);
    });
  });
}

export default function LogMetrics(server: http.Server, options: OPTIONS) {
  logConnections(server, options);
}
