import express from "express";
import aws from "aws-sdk";
import multer from "multer";
import multerS3 from "multer-s3";
import { v4 as uuidv4 } from "uuid";
import mime from "mime-types";

export type UPLOAD_CONFIG = {
  bucket: string;
  acl: "public-read";
};

export default (config: UPLOAD_CONFIG) => {
  const router = express.Router();

  const s3 = new aws.S3();
  const upload = multer({
    storage: multerS3({
      s3: s3,
      bucket: config.bucket,
      acl: config.acl,
      contentType: multerS3.AUTO_CONTENT_TYPE,
      key: function (_: any, file, cb) {
        const filenameUUID = uuidv4();
        const filename =
          filenameUUID.split("-")[0] +
          "/" +
          filenameUUID.split("-")[1] +
          "/" +
          filenameUUID +
          "." +
          mime.extension(file.mimetype);
        cb(null, filename);
      },
    }),
    limits: {
      fileSize: 10 * 1000000,
    },
  }).single("uploadfile");

  router.get("/", async (req, res) => {
    res.json({ home: true });
  });

  router.post("/", (req, res, next) => {
    upload(req, res, function (err: any) {
      if (err instanceof multer.MulterError && err.code === "LIMIT_FILE_SIZE") {
        return res.status(413).json({ error: "La taille des fichiers est limitée à 10Mb" });
      }

      if (err) {
        next(err);
        return;
      }

      const file = req.file as any;

      if (req.file) {
        return res.status(200).json({
          filename: file.key,
          contentType: file.contentType,
          mimetype: req.file.mimetype,
          size: req.file.size,
          // _original_file_upload : req.file
        });
      }
      return res.status(400).json({ msg: "PLEASE UPLOAD FILE" });
    });
  });

  return router;
};
