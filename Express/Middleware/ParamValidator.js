import { body, check, validationResult } from 'express-validator';
import ApiError from '../../Errors/ApiError';

export const validate = (req, res, next) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
        return next()
    }
    // [err.param]: err.msg

    console.log("ERRORS", errors.array());
    return res.status(422).json({
        error: {
            paramErrors: errors.array()
        },

    })
}

export default (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = errors.array()[0];
            if (!error.value) {
                throw new ApiError("Missing param "+ error.param, ApiError.CODE_PARAMETER_MISSING);
            } else {
                throw new ApiError(error.param + " : " + error.msg, ApiError.CODE_PARAMETER_FORMAT);
            }
        }
        next();
    } catch (error) {
        next(error);
    }
}