

export const ApiErrorHander = (err, req, res, next) => {
    console.log(err);
    if (err.name === 'ApiError') {
        res.json({
            error: true,
            type: err.name,
            code: err.code || -1,
            message: err.message
        })
        next();
        return;
    }
    next(err);
}