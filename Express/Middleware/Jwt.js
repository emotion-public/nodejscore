import ApiError from '../../Errors/ApiError';
import { verifyToken } from '../../JWT';


export const getTokenFromReq = (req) => {

    let token = null;
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token'];
    } else if (req.headers.authorization) {
        token = req.headers.authorization.substr("Bearer ".length);
    } else if (req.query.access_token) {
        token = req.query.access_token;
    } else if (req.query.token) {
        token = req.query.token;
    } else if (req.body.access_token) {
        token = req.query.access_token;
    } else if (req.body.token) {
        token = req.query.token;
    }

    return token;
}

export const NeedJwt = (req, res, next) => {
    if (res.locals.jwt_token) {
        next();
        return;
    }

    throw new ApiError("Need access_token");
}

export const getJwtGraphQlContext = (req) => {

    const token = getTokenFromReq(req);
    if (!token) {
        return {};
    }

    try {
        const decoded = verifyToken(token);

        if (decoded) {
            return {
                access_token: token,
                token,

                jwt_token: decoded,
                user_id: decoded.sub
            }
        }
    } catch (e) {
        if (e.name === 'JsonWebTokenError') {
            throw new ApiError("The token is not valid");
        }

        throw e;
    }
}

export const JWT = (req, res, next) => {
    const token = getTokenFromReq(req);
    if (!token) {
        next();
        return;
    }
    try {
        const decoded = verifyToken(token);

        if (decoded) {
            res.locals.access_token = token;
            res.locals.token = token;
            res.locals.jwt_token = decoded;
            res.locals.user_id = decoded.sub;
        }
        next();
    } catch (e) {
        if (e.name === 'JsonWebTokenError') {
            throw new ApiError("The token is not valid");
        }

        throw e;
    }
}