import mysql from "mysql";

type ValueType = string | number;
type FilterItem = () => [string, Array<ValueType>];

export default class FilterField {
  static create(fieldName: string): FilterField {
    return new FilterField(fieldName, "AND");
  }
  static createAnd(fieldName: string): FilterField {
    return new FilterField(fieldName, "AND");
  }
  static createOr(fieldName: string): FilterField {
    return new FilterField(fieldName, "OR");
  }

  private type: "AND" | "OR";
  private fieldName: string;
  private filters: Array<FilterItem>;

  constructor(fieldName: string, type: "AND" | "OR" = "AND") {
    this.fieldName = fieldName;
    this.filters = [];
    this.type = type;
  }

  getFieldName(): string {
    return this.fieldName;
  }
  setFieldName(fieldName: string) {
    this.fieldName = fieldName;
  }

  private getEscapedFieldName() {
    return mysql.escapeId(this.fieldName);
  }

  getQuery() {
    const query = this.filters
      .map((filter: FilterItem) => {
        return filter()[0];
      })
      .join(` ${this.type} `);

    if (this.filters.length > 1) {
      return `(${query})`;
    }
    return query;
  }
  getParams(): Array<ValueType> {
    return this.filters
      .map((filter: FilterItem) => {
        return filter()[1];
      })
      .reduce((acc, filterParams) => {
        return [...acc, ...filterParams];
      }, []);
  }

  isToday() {
    this.filters.push(() => [
      this.getEscapedFieldName() +
        " >= CURDATE() and " +
        this.getEscapedFieldName() +
        " < (CURDATE() + INTERVAL 1 DAY)",
      [],
    ]);
    return this;
  }

  isYesterday() {
    this.filters.push(() => [
      this.getEscapedFieldName() +
        " >= (CURDATE() - INTERVAL 1 DAY) and " +
        this.getEscapedFieldName() +
        " < CURDATE()",
      [],
    ]);
    return this;
  }

  isIn(values: Array<ValueType>) {
    this.filters.push(() => [
      this.getEscapedFieldName() + ` IN (${values.map((_: any) => "?").join(",")})`,
      values,
    ]);
    return this;
  }

  isEqualTo(value: ValueType) {
    this.filters.push(() => [this.getEscapedFieldName() + " = ?", [value]]);
    return this;
  }

  isNotEqualTo(value: ValueType) {
    this.filters.push(() => [this.getEscapedFieldName() + " != ?", [value]]);
    return this;
  }

  isTrue() {
    this.filters.push(() => [this.getEscapedFieldName() + " = true", []]);
    return this;
  }
  isFalse() {
    this.filters.push(() => [this.getEscapedFieldName() + " = false", []]);
    return this;
  }
  isNull() {
    this.filters.push(() => [this.getEscapedFieldName() + " is null", []]);
    return this;
  }
  isNotNull() {
    this.filters.push(() => [this.getEscapedFieldName() + " is not null", []]);
    return this;
  }


  isLike(value: ValueType) {
    this.filters.push(() => [this.getEscapedFieldName() + " like ?", [value]]);
    return this;
  }
  isNotLike(value: ValueType) {
    this.filters.push(() => [this.getEscapedFieldName() + " not like ?", [value]]);
    return this;
  }


  isGreaterThan(value: ValueType) {
    this.filters.push(() => [this.getEscapedFieldName() + " > ?", [value]]);
    return this;
  }
  isLessThan(value: ValueType) {
    this.filters.push(() => [this.getEscapedFieldName() + " < ?", [value]]);
    return this;
  }

  isGreaterOrEqualThan(value: ValueType) {
    this.filters.push(() => [this.getEscapedFieldName() + " >= ?", [value]]);
    return this;
  }
  isLessOrEqualThan(value: ValueType) {
    this.filters.push(() => [this.getEscapedFieldName() + " <= ?", [value]]);
    return this;
  }
}
