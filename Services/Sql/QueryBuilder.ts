import mysql from "mysql";
import Filter from "./Filter";
import FilterCombination from "./FilterCombination";

type DATA_TYPE = { [key: string]: any };

export default class QueryBuilder {
  private _queryType: "SELECT" | "UPDATE" | "CREATE" | "DELETE";
  private _data: DATA_TYPE;
  private _data_params: Array<any>;
  private _select: string;
  private _from: string;
  private _filters: Array<FilterCombination | Filter>;
  private _order: string;
  private _limit: [string, number];

  constructor() {
    this._filters = [];
    return this;
  }

  ///// PRIVATE

  ///// PUBLIC
  select(select: "*" | Array<string>) {
    let _select: string;
    if (select !== "*") {
      _select = select.map((field: string) => mysql.escapeId(field)).join(", ");
    } else {
      _select = select;
    }
    this._select = _select;
    this._queryType = "SELECT";
    return this;
  }

  selectCount(countFields: "*" | Array<string>, as?: string) {
    let _select: string;
    if (countFields !== "*") {
      _select = countFields.map((field: string) => mysql.escapeId(field)).join(", ");
    } else {
      _select = countFields;
    }
    this._select = `count(${_select})`;
    if (as) {
      this._select = this._select + " as " + mysql.escapeId(as);
    }
    this._queryType = "SELECT";
    return this;
  }

  update(data: DATA_TYPE) {
    this._data = data;
    this._data_params = Object.keys(data).map((key: string) => data[key]);
    this._queryType = "UPDATE";
    return this;
  }

  create(data: DATA_TYPE) {
    this._data = data;
    this._data_params = Object.keys(data).map((key: string) => data[key]);
    this._queryType = "CREATE";
    return this;
  }

  delete() {
    this._queryType = "DELETE";
    return this;
  }

  orderBy(fieldName: string, order: "ASC" | "DESC" = "ASC") {
    this._order = `order by ${mysql.escapeId(fieldName)} ${order}`;
    return this;
  }

  limit(limit: number) {
    this._limit = [`limit ?`, limit];
    return this;
  }

  from(table: string) {
    this._from = mysql.escapeId(table);
    return this;
  }

  where(filters: FilterCombination | Filter | Array<Filter>) {
    if (filters instanceof FilterCombination || filters instanceof Filter) {
      this._filters.push(filters);
    } else {
      this._filters = [...this._filters, ...filters];
    }
    return this;
  }

  private getCreateQueryMember() {
    const fieldNames = Object.keys(this._data)
      .map((key: string) => mysql.escapeId(key))
      .join(", ");
    const values = Object.keys(this._data)
      .map(() => "?")
      .join(", ");

    return `(${fieldNames}) VALUES (${values})`;
  }

  private getUpdateQueryMember() {
    return Object.keys(this._data)
      .map((key: string) => `${mysql.escapeId(key)} = ?`)
      .join(", ");
  }

  getQuery() {
    const query = [];

    switch (this._queryType) {
      case "SELECT":
        query.push("select " + this._select);
        query.push("from " + this._from);
        break;
      case "UPDATE":
        query.push("update " + this._from);
        query.push("set " + this.getUpdateQueryMember());
        break;
      case "CREATE":
        query.push("insert into " + this._from);
        query.push(this.getCreateQueryMember());
        break;
      case "DELETE":
        query.push("delete from " + this._from);
        break;
      default:
        throw new Error("no queryType ?");
    }

    if (this._queryType !== "CREATE") {
      const where = this._filters.map((filter: FilterCombination | Filter) => filter.getQuery());
      if (where.length > 0) {
        query.push("WHERE " + where.join(" AND "));
      }
    }

    if (this._queryType === "SELECT" && this._order) {
      query.push(this._order);
    }

    if (this._limit) {
      query.push(this._limit[0]);
    }

    return query.join(" ");
  }

  getParams(): Array<string | number> {
    let params = this._filters
      .map((filter: FilterCombination | Filter) => filter.getParams())
      .reduce((acc, filterParams) => {
        return [...acc, ...filterParams];
      }, []);

    params = [...params, ...(this._limit ? [this._limit[1]] : [])];

    if (this._queryType === "CREATE" || this._queryType === "UPDATE") {
      params = [...this._data_params, ...params];
    }

    return params;
  }
}
