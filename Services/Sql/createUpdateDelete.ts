import { query, QUERY_ARGS_TYPE } from "../../Db";
import Filter from "./Filter";
import QueryBuilder from "./QueryBuilder";

// export const create = async ({ table, data } = {}) => {
//   let KEYS = Object.keys(data).map((key) => `\`${key}\``);
//   let VALUES = Object.keys(data).map((_) => `?`);
//   const query = `INSERT INTO ${table} (${KEYS.join(", ")}) values (${VALUES.join(", ")})`;

//   const queryArgs = {
//     query,
//     params: [...Object.keys(data).map((key) => data[key])],
//   };

//   return await execQuery(queryArgs);
// };

export const _delete = async ({
  table,
  where: where_data,
  cache_key,
}: {
  table: string;
  where: any;
  cache_key?: string;
}) => {
  const queryBuilder = new QueryBuilder();

  queryBuilder
    .delete()
    .from(table)
    .where(
      Object.keys(where_data).map((keyName: string) =>
        Filter.createAnd(keyName).isEqualTo(where_data[keyName])
      )
    );

  const queryArgs: QUERY_ARGS_TYPE = {
    query: queryBuilder.getQuery(),
    query_type: "SET",
    params: queryBuilder.getParams(),
    cache_key,
  };

  return await query(queryArgs);
};

export const update = async ({
  table,
  where: where_data,
  data,
  cache_key,
}: {
  table: string;
  where: any;
  data: any;
  cache_key?: string;
}) => {
  const queryBuilder = new QueryBuilder();

  queryBuilder
    .update(data)
    .from(table)
    .where(
      Object.keys(where_data).map((keyName: string) =>
        Filter.createAnd(keyName).isEqualTo(where_data[keyName])
      )
    );

  const queryArgs: QUERY_ARGS_TYPE = {
    query: queryBuilder.getQuery(),
    query_type: "SET",
    params: queryBuilder.getParams(),
    cache_key,
  };

  return await query(queryArgs);
};
