import Filter from "./Filter";
import FilterCombination from "./FilterCombination";
import getListFull from "./getListFull";
import getListIds from "./getListIds";

export default async function getAll({
  table,
  primaryKey = "id",
  cursorKey = "id",

  after,
  first = 10,

  where,
  filters,
  filterDeleted = true,
  sqlFullSelect = false,
  reverseOrder = false,
}: {
  table: string;
  primaryKey?: string;
  cursorKey?: string;

  after?: string | null;
  first?: number;

  where: any;
  filters?: Array<Filter> | Filter | FilterCombination;
  filterDeleted?: boolean;
  sqlFullSelect?: boolean;
  reverseOrder?: boolean;
}) {
  if (sqlFullSelect) {
    return await getListFull({
      table,
      cursorKey,
      where,
      filters,
      filterDeleted,
      after,
      first,
      reverseOrder,
    });
  }
  return await getListIds({
    table,
    primaryKey,
    cursorKey,
    where,
    filters,
    filterDeleted,
    after,
    first,
    reverseOrder,
  });
}
