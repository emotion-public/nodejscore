import FilterField from "./Filter";

type ValueType = string | number;

export default class FilterCombination {
  static createAnd(): FilterCombination {
    return new FilterCombination("AND");
  }
  static createOr(): FilterCombination {
    return new FilterCombination("OR");
  }

  private type: "AND" | "OR";
  private filters: Array<FilterCombination | FilterField>;

  constructor(type: "AND" | "OR" = "AND") {
    this.filters = [];
    this.type = type;
  }

  setFilters(filters: Array<FilterCombination | FilterField>): FilterCombination {
    this.filters = filters;
    return this;
  }

  getFilters() {
    return [...this.filters];
  }

  addFilter(filter: FilterCombination | FilterField): FilterCombination {
    this.filters.push(filter);
    return this;
  }

  getQuery(): string {
    const query = this.filters
      .map((filter: FilterCombination | FilterField) => {
        return filter.getQuery();
      })
      .join(` ${this.type} `);

    if (this.filters.length > 1) {
      return `(${query})`;
    }
    return query;
  }
  getParams(): Array<ValueType> {
    return this.filters
      .map((filter: FilterCombination | FilterField) => {
        return filter.getParams();
      })
      .reduce((acc, filterParams) => {
        return [...acc, ...filterParams];
      }, []);
  }
}
