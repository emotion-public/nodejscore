import { query as execQuery, queryOne } from "../../Db";
import getAllV2 from "./getAll";

export const getCount = async ({ table, primaryKey = "id", where: where_data } = {}) => {
  let WHERE = Object.keys(where_data)
    .filter((key) => where_data[key] !== undefined)
    .map((key) => `\`${key}\` = ?`);

  // Filter by not deleted
  WHERE.push("`deleted_at` is null");

  let query = `SELECT count(\`${primaryKey}\`) as count FROM ${table}`;
  if (WHERE.length > 0) {
    query += ` where ${WHERE.join(" AND ")}`;
  }

  const queryParams = [
    ...Object.keys(where_data)
      .filter((key) => where_data[key] !== undefined)
      .map((key) => where_data[key]),
  ];

  const queryArgs = {
    query,
    params: queryParams,
  };

  const result = await queryOne(queryArgs);

  return result?.count || 0;
};

export const getListSelect = async ({ select_fields, table, primaryKey = "id", cursorKey = "id", where: where_data, after = "0", first = 10 } = {}) => {
  let WHERE = Object.keys(where_data)
    .filter((key) => where_data[key] !== undefined)
    .map((key) => `\`${key}\` = ?`);
  WHERE.push(cursorKey + " > ?");

  let SELECT_FIELDS = select_fields.map((select_field) => `\`${select_field}\``);

  // Filter by not deleted
  WHERE.push("`deleted_at` is null");

  const query = `SELECT \`${primaryKey}\`, \`${cursorKey}\`, ${SELECT_FIELDS.join(", ")} FROM ${table} where ${WHERE.join(" AND ")} limit ?`;

  const queryArgs = {
    query,
    params: [
      ...Object.keys(where_data)
        .filter((key) => where_data[key] !== undefined)
        .map((key) => where_data[key]),
      after,
      first,
    ],
  };

  return await execQuery(queryArgs);
};

export const getAll = async ({ table, primaryKey = "id", cursorKey = "id", after, first = 10, sqlFullSelect = false, filters, filtersV2 = undefined, reverseOrder = false } = {}) => {
  return await getAllV2({
    table,
    primaryKey,
    cursorKey,
    after,
    first,
    sqlFullSelect,
    reverseOrder,
    where: filters,
    filter: filtersV2,
  });
};

export const _delete = async ({ table, where: where_data, cache_key = undefined } = {}) => {
  let WHERE = Object.keys(where_data).map((key) => `\`${key}\` = ?`);
  const query = `DELETE FROM ${table} where ${WHERE.join(" AND ")}`;

  const queryArgs = {
    query,
    query_type: "DELETE",
    params: [...Object.keys(where_data).map((key) => where_data[key])],
    cache_key
  };

  return await execQuery(queryArgs);
};

export const create = async ({ table, data } = {}) => {
  let KEYS = Object.keys(data).map((key) => `\`${key}\``);
  let VALUES = Object.keys(data).map((_) => `?`);
  const query = `INSERT INTO ${table} (${KEYS.join(", ")}) values (${VALUES.join(", ")})`;

  const queryArgs = {
    query,
    params: [...Object.keys(data).map((key) => data[key])],
  };

  return await execQuery(queryArgs);
};

export const update = async ({ table, where: where_data, data } = {}) => {
  let SET = Object.keys(data).map((key) => `\`${key}\` = ?`);
  let WHERE = Object.keys(where_data).map((key) => `\`${key}\` = ?`);
  const query = `UPDATE ${table} SET ${SET.join(", ")} where ${WHERE.join(" AND ")}`;

  const params = [...Object.keys(data).map((key) => data[key]), ...Object.keys(where_data).map((key) => where_data[key])];
  const queryArgs = {
    query,
    params,
  };

  return await execQuery(queryArgs);
};
