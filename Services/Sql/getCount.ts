import { queryOne } from "../../Db";
import Filter from "./Filter";
import FilterCombination from "./FilterCombination";
import QueryBuilder from "./QueryBuilder";

export default async function getCount({
  table,
  primaryKey = "id",
  filters,
  where: where_data,
  cache_config,
}: {
  table: string;
  primaryKey: string;
  filters?: Array<Filter> | Filter | FilterCombination;
  where?: any;
  cache_config?: {
    key: string;
    expire: number;
  };
}) {
  const queryBuilder = new QueryBuilder();

  queryBuilder
    .selectCount([primaryKey], "count")
    .from(table)
    .where(filters || [])
    .where([Filter.createAnd("deleted_at").isNull()]);

  if (where_data) {
    Object.keys(where_data)
      .filter((key) => where_data[key] !== undefined)
      .forEach((key) => {
        `\`${key}\` = ?`;
        queryBuilder.where([Filter.createAnd(key).isEqualTo(where_data[key])]);
      });
  }

  const queryArgs = {
    query: queryBuilder.getQuery(),
    params: queryBuilder.getParams(),
    cache_config,
  };

  const result = await queryOne(queryArgs);

  return result?.count || 0;
}
