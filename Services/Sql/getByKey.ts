import { getCoreConfig } from "../../config";
import { query, queryOne } from "../../Db";
import { getClient as getMemcacheClient } from "../../Db/Memcache";
import Filter from "./Filter";
import QueryBuilder from "./QueryBuilder";

export const getByKey = async ({
  keyName,
  keyValue,
  table,
  cache_key,
}: {
  keyName: string;
  keyValue: any;
  table: string;
  cache_key?: string;
}) => {
  const queryBuilder = new QueryBuilder();

  queryBuilder
    .select("*")
    .from(table)
    .where([Filter.createAnd(keyName).isEqualTo(keyValue)])
    .limit(1);

  return await queryOne({
    query: queryBuilder.getQuery(),
    params: queryBuilder.getParams(),
    cache_key,
  });
};

type KEY_TYPE = {
  keyValue: any;
  cache_key?: string;
};

export const getByMultiKeys = async ({
  keyName,
  keys: _keys,
  table,
}: {
  keyName: string;
  keys: Array<KEY_TYPE>;
  table: string;
}) => {
  const { useDbCache } = getCoreConfig();

  const keys: Array<KEY_TYPE & { value?: any }> = [..._keys];
  let cachedDatas: { [key: string]: string } = {};
  const memcacheClient = getMemcacheClient();

  if (useDbCache) {
    const cache_keys: Array<string> = keys
      .map(({ cache_key }) => cache_key)
      .filter((cache_key: string | undefined) => !!cache_key) as Array<string>;

    if (cache_keys.length > 0) {
      try {
        cachedDatas = (await memcacheClient.getMulti(cache_keys)) || {};
      } catch (e: any) {}
    }
  }

  const queryBuilder = new QueryBuilder();
  queryBuilder.select("*").from(table);

  const values: Array<any> = [];
  keys.forEach(({ keyValue, cache_key }, index: number) => {
    if (!cache_key || !cachedDatas[cache_key]) {
      values.push(keyValue);
      return;
    }
    keys[index].value = JSON.parse(cachedDatas[cache_key]);
  });

  let dbResults = [];
  if (values.length > 0) {
    queryBuilder.where([Filter.create(keyName).isIn(values)]);

    dbResults = await query({
      query: queryBuilder.getQuery(),
      params: queryBuilder.getParams(),
    });

    dbResults.forEach(async (dbResult: any) => {
      const keyValueInDb = dbResult[keyName];
      const index = keys.findIndex(({ keyValue }) => {
        return keyValue === keyValueInDb;
      });
      keys[index].value = dbResult;
      const cache_key = keys[index].cache_key;
      if (useDbCache && !!cache_key) {
        try {
          await memcacheClient.set(cache_key, JSON.stringify(dbResult), 86400); // 1 DAY
        } catch (e: any) {}
      }
    });
  }

  return keys.map(({ value }) => value);
};
