import { query as execQuery } from "../../Db";
import Filter from "./Filter";
import FilterCombination from "./FilterCombination";
import QueryBuilder from "./QueryBuilder";

export default async function getListIds({
  table,
  primaryKey = "id",
  cursorKey = "id",
  where: where_data,
  filters,
  filterDeleted = true,
  after,
  first = 10,
  reverseOrder = false,
}: {
  table: string;
  primaryKey?: string;
  cursorKey?: string;
  where?: { [key: string]: any };
  filters?: Array<Filter> | Filter | FilterCombination;
  filterDeleted?: boolean;
  after?: string | null;
  first?: number;
  reverseOrder?: boolean;
}) {
  const queryBuilder = new QueryBuilder();

  queryBuilder
    .select([primaryKey, cursorKey])
    .from(table)
    .where(filters || [])
    .where(filterDeleted ? [Filter.createAnd("deleted_at").isNull()] : [])
    .limit(first);

  if (reverseOrder) {
    queryBuilder.orderBy(cursorKey, "DESC");
  }

  if (where_data) {
    Object.keys(where_data)
      .filter((key) => where_data[key] !== undefined)
      .forEach((key) => {
        `\`${key}\` = ?`;
        queryBuilder.where([Filter.createAnd(key).isEqualTo(where_data[key])]);
      });
  }

  if (after) {
    if (reverseOrder) {
      queryBuilder.where([Filter.createAnd(cursorKey).isLessThan(after)]);
    } else {
      queryBuilder.where([Filter.createAnd(cursorKey).isGreaterThan(after)]);
    }
  }

  const newQueryArgs = {
    query: queryBuilder.getQuery(),
    params: queryBuilder.getParams(),
  };

  return await execQuery(newQueryArgs);
}
