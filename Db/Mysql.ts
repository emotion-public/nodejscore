import { getCoreConfig } from "../config";
import mysql from "mysql";

let pool: mysql.Pool;

export const init = () => {
  const { mysqlConfig } = getCoreConfig();
  if (!mysqlConfig) {
    throw new Error("Missing mysqlConfig");
  }

  pool = mysql.createPool({
    connectionLimit: mysqlConfig.connectionLimit,
    host: mysqlConfig.host,
    port: mysqlConfig.port || 3306,
    user: mysqlConfig.user,
    password: mysqlConfig.password,
    database: mysqlConfig.database,
    dateStrings: true,
    charset: "utf8mb4",
  });
};

export const destroy = () => {
  return new Promise((resolve) => {
    pool.end(() => {
      resolve(true);
    });
  });
};

export const getClient = (): Promise<mysql.PoolConnection> => {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err, connection) {
      if (err) {
        reject(err);
      }
      resolve(connection);
    });
  });
};
