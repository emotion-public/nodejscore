import Memcached from "memcached";
import { getCoreConfig } from "../config";
Memcached.config.poolSize = 10;

let SERVERS: string;
let OPTIONS: any;

export const init = (_OPTIONS?: any) => {
  const { memcacheConfig } = getCoreConfig();
  if (!memcacheConfig) {
    return false;
  }
  const server = [memcacheConfig.host];
  if (memcacheConfig.port) {
    server.push(memcacheConfig.port);
  } else {
    server.push("11211");
  }
  SERVERS = server.join(":");
  if (_OPTIONS) {
    OPTIONS = _OPTIONS;
  }
  return true;
};

let sharedClient: Memcached;
export const getClient = (clientOptions = {}) => {
  if (!sharedClient) {
    sharedClient = new Memcached(SERVERS, {
      ...OPTIONS,
      ...clientOptions,
    });
  }
  return new MemcacheClient(sharedClient);
};

export function destroy() {
  if (!sharedClient) {
    return;
  }
  sharedClient.end();
}

export class MemcacheClient {
  private client: Memcached;
  constructor(client: Memcached) {
    this.client = client;
  }

  delete(key: string) {
    return new Promise((resolve, reject) => {
      this.client.del(key, function (error: any) {
        if (error) {
          reject(error);
        } else {
          resolve(true);
        }
      });
    });
  }

  get(key: string): Promise<string> {
    // eslint-disable-next-line
    if (process.env.APP_ENV !== "production") console.log("memcache -- get --", key);
    return new Promise((resolve, reject) => {
      this.client.get(key, function (error: any, data: any) {
        if (error) {
          reject(error);
        } else {
          resolve(data);
        }
      });
    });
  }

  getMulti(keys: Array<string>): Promise<{ [key: string]: string }> {
    // eslint-disable-next-line
    if (process.env.APP_ENV !== "production") console.log("memcache -- getmulti --", keys.length);
    return new Promise((resolve, reject) => {
      this.client.getMulti(keys, function (error: any, data: any) {
        if (error) {
          reject(error);
        } else {
          resolve(data as { [key: string]: string });
        }
      });
    });
  }

  set(key: string, value: string, lifetime = 15) {
    // eslint-disable-next-line
    if (process.env.APP_ENV !== "production") console.log("memcache -- set --", key);
    return new Promise((resolve, reject) => {
      this.client.set(key, value, lifetime, function (error: any) {
        if (error) {
          reject(error);
        } else {
          resolve(true);
        }
      });
    });
  }
}
