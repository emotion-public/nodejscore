import { Client } from '@elastic/elasticsearch'
import { getCoreConfig } from '../config';

let client;

function getConfig() {

    const { elasticsearchConfig } = getCoreConfig()
    const config = {
        node: elasticsearchConfig.node,
    }

    if (elasticsearchConfig.username && elasticsearchConfig.password) {
        config.auth = {
            username: elasticsearchConfig.username,
            password: elasticsearchConfig.password
        }
    }

    return config;
}

export function getClient() {
    return new Client(getConfig())
}

export default () => {
    if (client) {
        return client;
    }
    client = new Client(getConfig())
    return client;
}

