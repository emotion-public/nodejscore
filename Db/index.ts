import mysql from "mysql";
import { init as initSql, getClient as getMysqlClient, destroy as destroyMysql } from "./Mysql";
import {
  init as initMemcache,
  getClient as getMemcacheClient,
  MemcacheClient,
  destroy as destroyMemcache,
} from "./Memcache";
import DbError from "../Errors/DbError";
import { getCoreConfig } from "../config";

export type QUERY_ARGS_TYPE = {
  query: string;
  params?: any;
  cache_key?: string;
  query_type?: "DELETE" | "SET" | "GET";
  cache_config?: {
    key: string;
    expire: number;
  };
};

let INITIALIZED = false;
export const init = () => {
  if (INITIALIZED) {
    return false;
  }
  initSql();
  initMemcache();
  INITIALIZED = true;
};

export const getClient = async () => {
  const db = new Db();
  await db.init();
  return db;
};

export const test = async () => {
  try {
    const result = await query({ query: "select 1" });
    // eslint-disable-next-line
    console.log("test db result", result);
  } catch (e: any) {
    // eslint-disable-next-line
    console.log("ERROR TEST", e, e.code, e.errno);
    throw e;
  }
};

export const cleanup = async () => {
  destroyMemcache();
  await destroyMysql();
};

export const queryOne = async (args: QUERY_ARGS_TYPE) => query({ ...args, query_one: true });
export const query = async ({
  query,
  params,
  query_type,
  cache_key,
  cache_config,
  query_one = false,
}: QUERY_ARGS_TYPE & { query_one?: boolean }) => {
  let db;
  try {
    db = await getClient();
    const result = query_one
      ? await db.queryOne({ query, params, cache_config, cache_key })
      : await db.query({ query, query_type, params, cache_config, cache_key });
    db.release();
    return result;
  } catch (e: any) {
    if (db) {
      db.release();
    }

    if (e.code === "ER_DUP_ENTRY") {
      throw new DbError(e.message, DbError.CODE_DUPLICATE);
    }

    // eslint-disable-next-line
    console.log("ERROR_QUERY", query, params);
    // eslint-disable-next-line
    console.log(e.message, e.code);

    throw new DbError(e.message, e.code);
  }
};

class Db {
  private db: mysql.PoolConnection;
  private cache: MemcacheClient;

  release() {
    if (this.db) {
      this.db.release();
    }
  }

  destroy() {
    if (this.db) {
      this.db.destroy();
    }
  }

  async init() {
    init();
    this.db = await getMysqlClient();
    this.cache = getMemcacheClient();
  }

  async queryOne({ query, params, cache_config, cache_key: _cache_key }: QUERY_ARGS_TYPE) {
    const { useDbCache, dbCacheConfig } = getCoreConfig();
    const cache_key = cache_config?.key || _cache_key;

    const useCache = useDbCache && (dbCacheConfig?.useCacheForGet !== false);

    if (useCache && cache_key) {
      const cacheValue = await this.cache.get(cache_key);
      if (cacheValue) {
        // eslint-disable-next-line
        return JSON.parse(cacheValue);
      }
    }

    let result = await this.query({ query, params });
    if (!result[0]) {
      result = null;
    } else {
      result = result[0];
    }

    if (useCache && cache_key) {
      await this.cache.set(cache_key, JSON.stringify(result), cache_config?.expire || 86400); // 1 DAY
    }

    return result;
  }

  async query({ query, query_type, params, cache_config, cache_key: _cache_key }: QUERY_ARGS_TYPE) {
    const { useDbCache, dbCacheConfig } = getCoreConfig();

    const cache_key = cache_config?.key || _cache_key;

    const useCacheForGet = useDbCache && (dbCacheConfig?.useCacheForGet !== false);
    const useCacheForSet = useDbCache && (dbCacheConfig?.useCacheForSet !== false);

    if (query_type === "GET" && useCacheForGet && cache_key) {
      const cacheValue = await this.cache.get(cache_key);
      if (cacheValue) {
        return JSON.parse(cacheValue);
      }
    }

    const { results, error } = await new Promise((resolve) => {
      this.db.query(query, params, function (error, results) {
        resolve({ error, results });
      });
    });

    if (error) {
      throw error;
    }

    if (query_type === "GET" && useCacheForGet && cache_key) {
      await this.cache.set(cache_key, JSON.stringify(results), cache_config?.expire || 86400); // 1 DAY
    }
    if (query_type !== "GET" && useCacheForSet && cache_key) {
      await this.cache.delete(cache_key); // 1 DAY
    }

    return results;
  }
}
