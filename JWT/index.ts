import jwt from "jsonwebtoken";
import { getCoreConfig } from "../config";

let jwtVerifyFunction = (token: string, secret: string) => {
  return jwt.verify(token, secret);
};

export function setJwtVerifyFunction(_jwtVerifyFunction: (token: string, secret: string) => any) {
  jwtVerifyFunction = _jwtVerifyFunction;
}

export const createToken = (user_id: string): string => {
  const { jwtConfig } = getCoreConfig();

  const ISS = jwtConfig?.iss || "unknown";
  const SECRET = jwtConfig?.secret || "changeme";
  const EXPIRE_SECONDS = 3600 * 24 * parseInt(jwtConfig?.expireInDays || "730");

  const token = jwt.sign(
    {
      sub: user_id,
      iss: ISS,
      iat: Math.floor(Date.now() / 1000),
      exp: Math.floor(Date.now() / 1000) + EXPIRE_SECONDS,
    },
    SECRET
  );

  return token;
};

export const verifyToken = (token: string): boolean => {
  const { jwtConfig } = getCoreConfig();
  const SECRET = jwtConfig?.secret || "changeme";
  const OTHER_JWT_SECRETS = jwtConfig?.otherSecretsToCheck || [];

  const JWT_SECRETS = [SECRET, ...OTHER_JWT_SECRETS];
  const decodedToken = jwt.decode(token);
  if (typeof decodedToken !== "object") {
    throw new Error("Invalid token (could not decode)");
  }

  let decoded = null;
  JWT_SECRETS.some((JWT_SECRET: string) => {
    try {
      decoded = jwtVerifyFunction(token, JWT_SECRET);
      if (decoded) {
        return true;
      }
    } catch (e) {
      return false;
    }
  });

  if (!decoded) {
    throw new Error("Invalid Token");
  }

  return decoded;
};
