export type CONFIG = {
  debug?: {
    log?: boolean;
    testDb?: boolean;
  };

  firebase?: any;

  indexObjectInSearchEngine?: boolean;
  useDbCache?: boolean;
  dbCacheConfig?: {
    useCacheForGet?: boolean;
    useCacheForSet?: boolean;
  };

  jwtConfig?: {
    secret: string;
    iss: string;

    expireInDays?: string;
    otherSecretsToCheck?: Array<string>;
  };

  elasticsearchConfig?: {
    node: string;
    username?: string;
    password?: string;
  };
  memcacheConfig?: {
    host: string;
    port: string;
  };
  mysqlConfig?: {
    host: string;
    port?: number;
    database: string;
    user: string;
    password: string;
    connectionLimit: number;
  };
};

let _CONFIG: CONFIG = {};

export function getCoreConfig() {
  return {
    ..._CONFIG,
  };
}

export function setCoreConfig(_config: CONFIG) {
  _CONFIG = _config;
}
