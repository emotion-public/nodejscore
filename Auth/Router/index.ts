import express from "express";
import firebaseRouter from "./FirebaseAuth";

const router = express.Router();
router.use("/firebase", firebaseRouter);

export default router;
