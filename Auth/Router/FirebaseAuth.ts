import express from "express";
import User from "../../../Models/User";
import { registerWithPhoneNumber } from "../../../Services/User/register";
import { verifyToken } from "../../Libs/Firebase";
import Filter from "../../Services/Sql/Filter";
import { createToken } from "../../JWT";

const router = express.Router();

function errorAuth(res: any, message?: string) {
  return res.json({ error: true, message: message || "Unknown error" });
}

router.post("/withToken", async (req, res) => {
  if (!req.body.idToken) {
    return errorAuth(res, "No token");
  }

  const result = await verifyToken(req.body.idToken);

  if (!result.phone_number || !result.user_id) {
    return errorAuth(res, "missing phone_number or user_id");
  }

  let user: User;
  try {
    user = await User.getOne({
      filtersV2: [Filter.create("phone").isEqualTo(result.phone_number)],
    });
  } catch (e: any) {
    user = await registerWithPhoneNumber({ phone: result.phone_number });
  }

  const access_token = createToken(String((user as any)[User.dbConf.primaryKey]));

  return res.json({ access_token });
});

export default router;
