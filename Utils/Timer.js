export default class Timer {
  constructor() {
    this.timer = null;
  }

  start() {
    this.timer = new Date().getTime();
  }

  getTimeSpent = () => {
    return (new Date().getTime()) - this.timer;
  };
}