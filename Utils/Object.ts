export const filterObject = (obj: { [key: string]: any }, attrs: true | { [key: string]: any }) => {
  if (attrs === true) {
    return obj;
  }
  const filteredObject: { [key: string]: any } = {};
  Object.keys(attrs).forEach((attr) => {
    // eslint-disable-next-line
    if (!obj.hasOwnProperty(attr)) {
      return;
    }
    filteredObject[attr] = filterObject(obj[attr], attrs[attr]);
  });
  return filteredObject;
};

export const ObjectMapKeys = (
  filters: { [key: string]: any } = {},
  mapping: { [key: string]: any }
) => {
  const sqlFilter: { [key: string]: any } = {};

  Object.keys(filters).map((key: string) => {
    if (!mapping[key]) {
      return;
    }
    sqlFilter[mapping[key]] = filters[key];
  });

  return sqlFilter;
};
