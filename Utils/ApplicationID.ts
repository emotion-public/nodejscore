export default function getUniqAppId(): string {
  if (process.env.NODEJS_APP_ID) {
    return process.env.NODEJS_APP_ID;
  }

  const appId = [];
  if (process.env.NODE_NAME) {
    appId.push(process.env.NODE_NAME);
  }
  if (process.env.NAMESPACE_NAME) {
    appId.push(process.env.NAMESPACE_NAME);
  }
  if (process.env.POD_NAME) {
    appId.push(process.env.POD_NAME);
  }

  if (appId.length > 0) {
    return appId.join(".");
  }

  return "unknown";
}
