const $characters = "0123456789ABCDEFGHIJKLM";
const $charactersLength = $characters.length;

export function generateNewRandomString($length = 10): string {
  let $randomString = "";
  for (let i = 0; i < $length; i++) {
    $randomString += $characters[Math.floor(Math.random() * ($charactersLength - 1))];
  }
  return $randomString;
}
