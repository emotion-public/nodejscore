import { sendToCloudWatch, CLOUDWATCH_CONFIG } from "./CloudWatch";

type LOG_SERVICE = "cloudwatch";

export type LOG_CONFIG = {
  logEnabled?: boolean;
  logService?: LOG_SERVICE;
  cloudWatchLogConfig?: {
    region: string;
    namespace: string;
  };
};
export type OPTIONS = {
  uniqAppId: string;
  logService?: LOG_SERVICE;
  cloudWatchConfig?: CLOUDWATCH_CONFIG;
};

export function sendLogNumber(MetricName: string, Value: number, options: OPTIONS) {
  if (options.logService === "cloudwatch" && options.cloudWatchConfig) {
    return sendToCloudWatch({
      MetricData: [
        {
          MetricName,
          Value,
          Dimensions: [{ Name: "NodeId", Value: options.uniqAppId }],
        },
      ],
      cloudWatchConfig: options.cloudWatchConfig,
    });
  }
}
