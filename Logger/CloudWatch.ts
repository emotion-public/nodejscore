import aws from "aws-sdk";

export type CLOUDWATCH_CONFIG = {
  namespace: string;
  region: string;
};
export type OPTIONS = {
  uniqAppId: string;
  logService?: "cloudwatch";
  cloudWatchConfig?: CLOUDWATCH_CONFIG;
};

export function sendToCloudWatch({
  MetricData,
  cloudWatchConfig,
}: {
  MetricData: Array<{
    MetricName: string;
    Value: number;
    Unit?:
      | "Seconds"
      | "Microseconds"
      | "Milliseconds"
      | "Bytes"
      | "Kilobytes"
      | "Megabytes"
      | "Gigabytes"
      | "Terabytes"
      | "Bits"
      | "Kilobits"
      | "Megabits"
      | "Gigabits"
      | "Terabits"
      | "Percent"
      | "Count"
      | "Bytes/Second"
      | "Kilobytes/Second"
      | "Megabytes/Second"
      | "Gigabytes/Second"
      | "Terabytes/Second"
      | "Bits/Second"
      | "Kilobits/Second"
      | "Megabits/Second"
      | "Gigabits/Second"
      | "Terabits/Second"
      | "Count/Second"
      | "None";
    Dimensions?: Array<{ Name: string; Value: string }>;
  }>;

  cloudWatchConfig: CLOUDWATCH_CONFIG;
}) {
  const cloudwatch = new aws.CloudWatch({
    region: cloudWatchConfig.region,
  });

  cloudwatch.putMetricData(
    {
      MetricData,
      Namespace: cloudWatchConfig.namespace,
    },
    (err) => {
      if (err) {
        // eslint-disable-next-line
        console.log("Failed putting metricsdata to cloudwatch");
        // eslint-disable-next-line
        console.log(err);
      }
    }
  );
}
