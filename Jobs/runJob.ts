/* eslint-disable no-debugger, no-console */

import arg from "arg";
import fs from "fs";
import { AbstractJob } from "./AbstractJob";

async function runJob() {
  const args = arg(
    {
      "--job-name": [String],
    },
    {
      permissive: true,
    }
  );

  const scriptName = args["--job-name"];

  if (!scriptName) {
    throw new Error("No job name provided -- --job-name");
  }

  console.log("scriptName", scriptName, __dirname);

  let requirePath;
  const paths = [
    `./${scriptName}.ts`,
    `./${scriptName}.js`,
    `../../Jobs/${scriptName}.ts`,
    `../../Jobs/${scriptName}.js`,
  ].map((path: string) => __dirname + "/" + path);
  for (let index = 0; index < paths.length; index++) {
    const path = paths[index];
    if (fs.existsSync(path)) {
      requirePath = path;
      break;
    }
  }

  if (!requirePath) {
    throw new Error("No Job found " + paths.join("\n"));
  }

  // eslint-disable-next-line
  const UserJob = require(requirePath).default;

  if (!UserJob.prototype || !(UserJob.prototype instanceof AbstractJob)) {
    throw new Error("Job cannot be run");
  }

  const job = new UserJob();
  console.log("args._", args._);
  job.setArgs(
    arg(UserJob.getArgs(), {
      argv: args._,
    })
  );
  console.log("Running job from file : ", requirePath);
  job.setStartTime(new Date().getTime());
  await job.preRun();
  try {
    await job.run();
  } catch (e: any) {
    job.logLine("", true);
    throw e;
  }
  await job.postRun();
}

runJob();
