export type ARGS = { [key: string]: any };

export abstract class AbstractJob {
  static getArgs(): ARGS {
    return {};
  }

  abstract preRun(): Promise<boolean>;
  abstract run(): Promise<boolean>;
  abstract postRun(): Promise<boolean>;

  startTime: number = 0;

  setStartTime(time: number) {
    this.startTime = time;
  }

  logLine(text: string, addNewLine: boolean) {
    const elapsedTimeSecond = Math.floor(((new Date().getTime()) - this.startTime) / 10) / 10;
    process.stdout.write(text + ` --- [memory : ${process.memoryUsage().heapUsed/1000000}MB | ElapsedTime : ${elapsedTimeSecond} ] ---` + (addNewLine?"\n":"\r"));
  }

  protected _args: ARGS;
  setArgs(args: ARGS) {
    this._args = args;
  }
}
