import { OPTIONS } from "../Logger";
import { sendToCloudWatch } from "../Logger/CloudWatch";

export default function getLogPlugin(options: OPTIONS): any {
  return {
    requestDidStart(_: any): any {
      const start = Date.now();
      let op: string;

      return {
        async didResolveOperation(context: any) {
          op = context.operationName || "";
        },

        async willSendResponse(context: any) {
          const stop = Date.now();
          const elapsed = stop - start;
          const size = JSON.stringify(context.response).length * 2;
          // eslint-disable-next-line
          console.log(`operataion=${op} duration=${elapsed}ms bytes=${size}`);

          if (!op) {
            op = "unknown";
          }

          if (options.logService === "cloudwatch" && options.cloudWatchConfig) {
            sendToCloudWatch({
              MetricData: [
                // ALL GQL RQ
                {
                  MetricName: "count_graphql_rq",
                  Value: 1,
                  Unit: "Count",
                },
                // GQL RQ / Node
                // {
                //   MetricName: "count_graphql_rq",
                //   Value: 1,
                //   Unit: "Count",
                //   Dimensions: [
                //     {
                //       Name: "NodeId",
                //       Value: getUniqAppId(),
                //     },
                //   ],
                // },
                // GQL RQ / QUERY
                {
                  MetricName: "count_graphql_rq",
                  Value: 1,
                  Unit: "Count",
                  Dimensions: [
                    {
                      Name: "GQL_QUERY_NAME",
                      Value: op,
                    },
                  ],
                },
                // GQL RQ duration / QUERY
                {
                  MetricName: "request_duration",
                  Value: elapsed,
                  Unit: "Milliseconds",
                  Dimensions: [
                    {
                      Name: "GQL_QUERY_NAME",
                      Value: op,
                    },
                  ],
                },
                // // GQL RQ / Node / QUERY
                // {
                //   MetricName: "count_graphql_rq",
                //   Value: 1,
                //   Unit: "Count",
                //   Dimensions: [
                //     {
                //       Name: "GQL_QUERY_NAME",
                //       Value: op,
                //     },
                //     {
                //       Name: "NodeId",
                //       Value: getUniqAppId(),
                //     },
                //   ],
                // },
                // // GQL RQ duration / Node / QUERY
                // {
                //   MetricName: "request_duration",
                //   Value: elapsed,
                //   Unit: "Milliseconds",
                //   Dimensions: [
                //     {
                //       Name: "GQL_QUERY_NAME",
                //       Value: op,
                //     },
                //     {
                //       Name: "NodeId",
                //       Value: getUniqAppId(),
                //     },
                //   ],
                // },
              ],
              cloudWatchConfig: options.cloudWatchConfig,
            });
          }
        },
      };
    },
  };
}
