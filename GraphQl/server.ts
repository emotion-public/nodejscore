import { Express } from "express";
import { gql, ApolloServer, ApolloServerExpressConfig } from "apollo-server-express";
import { DocumentNode } from "graphql";
import depthLimit from "graphql-depth-limit";
import { StringOrIntResolver } from "./Utils/StringOrIntValue";
import { getJwtGraphQlContext } from "../Express/Middleware/Jwt";
import getLogPlugin from "./LogPlugin";
import getUniqAppId from "../Utils/ApplicationID";

const baseTypeDefs = gql`
  type Query {
    root: String
  }
  type Mutation {
    root: String
  }

  scalar StringOrInt
`;

export default async function createServer({
  app,
  apolloServerConfig,
  getContext,

  logConfig = {
    logEnabled: false,
  },
}: {
  app: Express;
  apolloServerConfig: ApolloServerExpressConfig;
  getContext: (args: any) => any;

  logConfig?: {
    logEnabled?: boolean;
    logService?: "cloudwatch";
    cloudWatchLogConfig?: {
      region: string;
      namespace: string;
    };
  };
}) {
  // eslint-disable-next-line
  console.log("Start Graph Server with log config : ", logConfig);

  const server = new ApolloServer({
    tracing: true,
    plugins: logConfig?.logEnabled
      ? [
          getLogPlugin({
            uniqAppId: getUniqAppId(),
            logService: logConfig.logService,
            cloudWatchConfig: logConfig.cloudWatchLogConfig,
          }),
        ]
      : [],

    typeDefs: [baseTypeDefs, ...((apolloServerConfig.typeDefs || []) as Array<DocumentNode>)],
    resolvers: {
      ...(apolloServerConfig.resolvers || {}),
      StringOrInt: StringOrIntResolver,
    },

    schemaDirectives: apolloServerConfig.schemaDirectives,
    validationRules: [depthLimit(10)],

    context: async ({ req }) => {
      const jwtGraphContext = getJwtGraphQlContext(req);

      let context = {
        ...jwtGraphContext,
      };

      if (getContext) {
        context = getContext({
          context,
          req,
        });
      }

      return context;
    },
  });
  server.applyMiddleware({ app });
  return server;
}
