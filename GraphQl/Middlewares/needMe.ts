export default async function MustBeMeMiddleware(ob: any, args: any, context: any) {
  if (!context.user) {
    throw new Error("Not authorized");
  }

  if (context.user.id !== ob.id) {
    throw new Error("Not authorized");
  }

  return true;
}
