export default async function MustBeAdminMiddleware(ob: any, args: any, context: any) {
  if (!context.user) {
    throw new Error("Not authorized");
  }

  if (!context.user.isAdmin()) {
    throw new Error("Not authorized");
  }

  return true;
}
