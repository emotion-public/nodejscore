import { UserInputError } from "apollo-server-express";
import { GraphQLScalarType, Kind } from "graphql";

export default function StringOrIntValue(value: number | string) {
  if ((typeof value === "number" && Number.isInteger(value)) || typeof value === "string") {
    return value;
  }
  throw new UserInputError("Provided value is not a string nor an int");
}

export const StringOrIntResolver = new GraphQLScalarType({
  name: "StringOrInt",
  description: "StringOrInt custom scalar type - String | Int",
  serialize: StringOrIntValue,
  parseValue: StringOrIntValue,
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      return parseInt(ast.value, 10);
    }
    if (ast.kind === Kind.STRING) {
      return ast.value;
    }
    throw new UserInputError("Provided value is not a string nor an int");
  },
});
