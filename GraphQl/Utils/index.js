


export const ApplyAccessFieldResolverMiddlewares = (middlewares, resolver) => async (ob, args, context) => {
    for (var index = 0; index < middlewares.length; index++) {
        try {
            if (!await middlewares[index](ob, args, context)) {
                return null;
            }
        } catch (e) {
            return null;
        }
    }

    return resolver(ob, args, context);
}

export const ApplyResolverMiddlewares = (middlewares, resolver) => async (ob, args, context) => {
    for (var index = 0; index < middlewares.length; index++) {
        await middlewares[index](ob, args, context)
    }

    return resolver(ob, args, context);
}

export const getByLoader = (loader_name, key, transform) => async (ob, args, { dataloaders }) => {

    if (!ob[key]) {
        return null;
    }

    const obj = await dataloaders[loader_name].load(ob[key]);
    return transform ? transform(obj) : obj;
}

export const ListResolver = (loader, getCursor) => async (parent, { first = 10, ...args } = {}, context) => {

    let data = await loader(parent, { first: first+1, ...args }, context);

    let hasNextPage = false;
    if (data.length === first + 1) {
        data = data.splice(0, data.length-1);
        hasNextPage = true;
    }

    return {
        pageInfo: {
            hasNextPage,
            endCursor: String(data.length ? getCursor(data[data.length-1]) : 0)
        },
        edges: data.map((node, i) => ({
            cursor: String(getCursor(node)),
            node
        }))
    };
}