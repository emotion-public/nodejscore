import { ApolloError } from "apollo-server-express";

export class NotAuthorized extends ApolloError {
  constructor(message: string) {
    super(message, "OPERATION_NOT_AUTHORIZED");
    Object.defineProperty(this, "name", { value: "NotAuthorized" });
  }
}
