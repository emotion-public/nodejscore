import admin from "firebase-admin";

let sharedInstance: admin.app.App;

export function initSharedInstance(firebaseConfig: any) {
  sharedInstance = admin.initializeApp({
    credential: admin.credential.cert(firebaseConfig),
  });
}

export async function verifyToken(token: string) {
  const result = await sharedInstance.auth().verifyIdToken(token);
  return result;
}
