export default class ApiError extends Error {

    static CODE_UNKNOWN = -1;
    static CODE_NO_TOKEN = 100;

    static CODE_PARAMETER_MISSING = 20;
    static CODE_PARAMETER_FORMAT = 21;

    name = "ApiError"

    constructor(message, code = -1) {
        super(message);
        this.code = code;
    }
}