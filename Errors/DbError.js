export default class DbError extends Error {

    static CODE_UNKNOWN = -1;

    static CODE_DUPLICATE = 100;

    name = "DbError"

    constructor(message, code = -1) {
        super(message);
        this.code = code;
    }
}