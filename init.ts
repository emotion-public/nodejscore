import { JwtPayload } from "jsonwebtoken";
import { CONFIG, getCoreConfig, setCoreConfig } from "./config";
import { init as initDb, test as testDb } from "./Db";
import { setJwtVerifyFunction } from "./JWT";

import { initSharedInstance } from "./Libs/Firebase";

export default async function initCore({
  config: _config,
  jwtVerifyFunction,
}: {
  config: CONFIG;
  jwtVerifyFunction?: (token: string, secret: string) => JwtPayload;
}) {
  // eslint-disable-next-line
  if (_config?.debug?.log) console.log("INIT With config : ", _config);
  setCoreConfig(_config);
  if (jwtVerifyFunction) {
    setJwtVerifyFunction(jwtVerifyFunction);
  }

  const config = getCoreConfig();

  if (config?.firebase) {
    // initializeApp
    initSharedInstance(config.firebase);
  }

  if (config?.debug?.log) {
    // eslint-disable-next-line no-console
    console.log("DB_DATABASE_MYSQL", config.mysqlConfig?.host, ">", config.mysqlConfig?.database);
    // eslint-disable-next-line no-console
    console.log("SENDGRID_API_KEY", process.env.SENDGRID_API_KEY);
  }

  initDb();
  if (config?.debug?.testDb) {
    await testDb();
  }
}
