import "reflect-metadata";
import DataLoader from "dataloader";
import dateformat from "dateformat";
import getEsClient from "../Db/Elasticsearch";
import { create, _delete } from "../Services/Sql/Common";
import getAll from "../Services/Sql/getAll";
import getCount from "../Services/Sql/getCount";
import { ObjectMapKeys } from "../Utils/Object";
import Filter from "../Services/Sql/Filter";
import FilterCombination from "../Services/Sql/FilterCombination";
import { getCoreConfig } from "../config";
import { getByKey, getByMultiKeys } from "../Services/Sql/getByKey";
import { update } from "../Services/Sql/createUpdateDelete";
import { filterFiltersWithSqlMapping } from "./FilterApplySqlMappingHelper";

export const bindDbAttributeBlobTextOptions = {
  dbToObject: (dbValue: any) => {
    try {
      if (typeof dbValue === "object" && dbValue.data) {
        return Buffer.from(dbValue.data).toString();
      }
      return dbValue;
    } catch (e: any) {
      return undefined;
    }
  },
  objectToDb: (objValue: any) => {
    return objValue;
  },
};

export const bindDbAttributeJsonOptions = {
  dbToObject: (dbValue: any) => {
    try {
      if (typeof dbValue === "object" && dbValue.data) {
        return JSON.parse(Buffer.from(dbValue.data).toString());
      }
      return JSON.parse(dbValue);
    } catch (e: any) {
      return undefined;
    }
  },
  objectToDb: (objValue: any) => {
    return JSON.stringify(objValue);
  },
};

export const bindDbAttributeDateOptions = {
  dbToObject: (dbValue: any) => {
    return new Date(dbValue).getTime() / 1000;
  },
  objectToDb: (objValue: any) => {
    if (objValue === null) {
      return null;
    }
    return dateformat(new Date(objValue * 1000), "UTC:yyyy-mm-dd HH:MM:ss");
  },
};

export const exportable = (target: any, propertyKey: string, _?: any) => {
  if (!target.constructor.exportFields) {
    target.constructor.exportFields = [];
  }

  target.constructor.exportFields.push(propertyKey);
};

export function enumerable(isEnumerable: boolean) {
  return function (target: any, propertyKey: string, _?: any) {
    Object.defineProperty(target, propertyKey, {
      set(value) {
        const descriptor = Object.getOwnPropertyDescriptor(this, propertyKey) || {};

        Object.defineProperty(this, propertyKey, {
          value,
          writable: true,
          configurable: true,
          ...descriptor,
          enumerable: isEnumerable,
        });
      },
      enumerable: isEnumerable,
      configurable: true,
    });
  };
}

const autocastToType = (type: any, value: any): any => {
  if (value === null || value === undefined) {
    return value;
  }

  switch (true) {
    case type === String:
      return String(value);
    case type === Boolean:
      return Boolean(value);
    case type === Number: {
      const returnVal = Number(value);
      if (isNaN(returnVal)) {
        return undefined;
      }
      return returnVal;
    }

    case typeof type === "function" && type.constructor && typeof type.constructor === "function": {
      if (value instanceof type) {
        return value;
      }
      const castedValue = new type();
      if (castedValue instanceof ModelObject && typeof value === "object") {
        castedValue.setData(value);
      }
      return castedValue;
    }
    default:
      return value;
  }
};

export const autocast = (target: any, propertyKey: string, _?: any) => {
  const type = Reflect.getMetadata("design:type", target, propertyKey);

  Object.defineProperty(target, propertyKey, {
    enumerable: true,
    configurable: true,
    set(initialValue: any) {
      if (!this[propertyKey + "__values"]) {
        Object.defineProperty(this, propertyKey + "__values", {
          value: new Map<any, any>(),
          configurable: true,
          enumerable: false,
        });
      }
      Object.defineProperty(this, propertyKey, {
        set(value: any) {
          this[propertyKey + "__values"].set(this, autocastToType(type, value));
        },
        get() {
          return this[propertyKey + "__values"].get(this);
        },
        enumerable: true,
      });
      this[propertyKey] = initialValue;
    },
  });
};

export const bindDbAttribute =
  (
    _dbField?: string,
    serializers?: {
      // eslint-disable-next-line
      dbToObject: Function;
      // eslint-disable-next-line
      objectToDb: Function;
    }
  ) =>
  (target: any, propertyKey: string, _?: any) => {
    const dbField = _dbField || propertyKey;

    if (!target.constructor.dbFieldMapping) {
      target.constructor.dbFieldMapping = {};
    }

    target.constructor.dbFieldMapping[dbField] = propertyKey;

    if (!target.constructor.dbFieldSerializers) {
      target.constructor.dbFieldSerializers = {};
    }

    target.constructor.dbFieldSerializers[dbField] = serializers;
  };

export interface IIndexable {
  [key: string]: any;
}

export type DbConfType = {
  table?: string;
  searchIndex?: string;
  primaryKey?: string;
  cache_prefix?: string;
  cursorKey?: string;
  sqlMapping?: { [key: string]: any };
};

export class ModelObject {
  static exportFields = [];
  // static dbFieldMapping = {} // { keyDb : propertyName }
  static dbConf: DbConfType = {};

  test() {
    // eslint-disable-next-line
    console.log((<any>this.constructor).dbFieldMapping);
  }

  setData(data: any) {
    const objectProperties = Object.getOwnPropertyNames((<any>this.constructor).prototype);
    Object.keys(data).forEach((key) => {
      if (objectProperties.indexOf(key) !== -1) {
        Object.assign(this, { [key]: data[key] });
      }
    });
  }

  setDbData(data: any) {
    const dbFieldMapping = (<any>this.constructor).dbFieldMapping;
    const dbFieldSerializers = (<any>this.constructor).dbFieldSerializers;

    Object.keys(data).forEach((keyDb) => {
      const keyObj = dbFieldMapping[keyDb];
      if (!keyObj) {
        return true;
      }

      let value = data[keyDb];
      if (value === null) {
        return true;
      }

      if (dbFieldSerializers[keyDb] && dbFieldSerializers[keyDb].dbToObject) {
        value = dbFieldSerializers[keyDb].dbToObject(data[keyDb]);
      }

      Object.assign(this, { [keyObj]: value });
    });
  }

  getDbFields({
    excludeDbFields,
    includeDbFields,
  }: {
    excludeDbFields?: [string];
    includeDbFields?: [string];
  } = {}): IIndexable {
    const dbFieldMapping = (<any>this.constructor).dbFieldMapping;
    const dbFieldSerializers = (<any>this.constructor).dbFieldSerializers;
    const dbFields = {} as IIndexable;
    const currentObj = this as IIndexable;

    Object.keys(dbFieldMapping).forEach((keyDb: string) => {
      if (excludeDbFields && excludeDbFields.indexOf(keyDb) !== -1) {
        return;
      }
      if (includeDbFields && includeDbFields.indexOf(keyDb) === -1) {
        return;
      }
      const keyObj = dbFieldMapping[keyDb];

      if (currentObj[keyObj] === undefined) {
        return;
      }

      if (dbFieldSerializers[keyDb] && dbFieldSerializers[keyDb].objectToDb) {
        dbFields[keyDb] = dbFieldSerializers[keyDb].objectToDb(currentObj[keyObj]);
      } else {
        dbFields[keyDb] = currentObj[keyObj];
      }
    });

    return dbFields;
  }

  static getPropertyFromDbField(dbField: string): string {
    const dbFieldMapping = (<any>this).dbFieldMapping;

    return dbFieldMapping[dbField] || null;
  }

  // eslint-disable-next-line
  async exportForEs<T extends ModelObject>({
    dataLoaders: _ = {},
  }: {
    dataLoaders?: any;
  } = {}): Promise<any> {
    const obj = { ...this } as any;
    // eslint-disable-next-line
    const { _id, ...data } = obj;

    return {
      ...data,
      created_at: obj.created_at
        ? dateformat(new Date(obj.created_at * 1000), "yyyy-mm-dd HH:MM:ss")
        : null,
      updated_at: obj.updated_at
        ? dateformat(new Date(obj.updated_at * 1000), "yyyy-mm-dd HH:MM:ss")
        : null,
    };
  }

  export() {
    const exportableFields = (<any>this.constructor).exportFields;
    const exportedFields = {} as IIndexable;
    const currentObj = this as IIndexable;
    exportableFields.map((key: string) => {
      exportedFields[key] = currentObj[key];
    });

    return exportedFields;
  }

  checkDbConf() {
    const dbConf = (<any>this.constructor).dbConf;
    if (!dbConf.table || !dbConf.primaryKey) {
      throw new Error("dbConf table or primaryKey missing !");
    }
  }

  static getAllKeysDataKeyExtractor(_: any): any {
    throw new Error("You must implement getAllKeysDataKeyExtractor in your class");
  }

  static getSqlFilterMapping(): any {
    throw new Error("You must implement getSqlFilterMapping in your class");
  }

  static getTable(): string {
    throw new Error("You must implement getTable in your class");
  }

  static async getAllKeys({
    after,
    first,
    where,
    filters,
    filterDeleted = true,
    reverseOrder,
  }: {
    after?: string | null;
    first: number;
    where: any;
    filters?: Array<Filter> | Filter | FilterCombination;
    filterDeleted?: boolean;
    reverseOrder?: boolean;
  }) {
    const { table, primaryKey, cursorKey, sqlMapping } = this.dbConf;

    if (!table) {
      throw new Error("Table is not defined");
    }

    return (
      await getAll({
        table,
        primaryKey,
        cursorKey,
        after,
        first,
        filters: filterFiltersWithSqlMapping(filters, sqlMapping),
        filterDeleted,
        where: sqlMapping ? ObjectMapKeys(where, sqlMapping) : {},
        reverseOrder,
      })
    ).map(this.getAllKeysDataKeyExtractor);
  }

  static async getAllFullSelect({
    after,
    first,
    where,
    filters,
    filterDeleted = true,
    reverseOrder,
  }: {
    after?: string | null;
    first: number;
    where?: any;
    filters?: Array<Filter> | Filter | FilterCombination;
    filterDeleted?: boolean;
    reverseOrder?: boolean;
  }) {
    const dbConf = this.dbConf;
    const table = dbConf.table;
    if (!table) {
      throw new Error("dbConf.table is not defined");
    }
    const getAllOptions = {
      table,
      after,
      first,
      reverseOrder,
      sqlFullSelect: true,
      filters: filterFiltersWithSqlMapping(filters, dbConf.sqlMapping),
      filterDeleted,
      where: where ? (dbConf.sqlMapping ? ObjectMapKeys(where, dbConf.sqlMapping) : where) : {},
    };

    return (await getAll(getAllOptions)).map((data: any) => {
      const obj = new this();
      obj.setDbData(data);
      return obj;
    });
  }

  static async getAllFromKeys<T extends ModelObject>({
    keys,
    load = false,
    dataLoader,
  }: {
    keys: Array<any>;
    load?: boolean;
    dataLoader?: DataLoader<any, T>;
  }): Promise<Array<T>> {
    const dbConf = this.dbConf;

    if (!dbConf.primaryKey) {
      throw new Error("Missing primary key definition");
    }

    const objects = await Promise.all(
      keys.map(async (key: any) => {
        try {
          if (dataLoader) {
            return (await dataLoader.load(key)) as T;
          }
          const obj = new this() as T;
          obj.setData({
            [dbConf.primaryKey!]: key,
          });
          if (load) {
            await obj.load();
          }
          return obj;
        } catch (e: any) {
          // eslint-disable-next-line no-console
          console.log("ERROR", key, e.message);
          return null;
        }
      })
    );
    const objectFiltered: Array<T> = [];

    for (let index = 0; index < objects.length; index++) {
      const object = objects[index];
      if (object !== null) {
        objectFiltered.push(object);
      }
    }

    return objectFiltered;
  }

  static async exists<T extends ModelObject>({
    filters,
  }: {
    filters?: Array<Filter> | Filter | FilterCombination;
  }) {
    try {
      await this.getOne<T>({
        load: false,
        sqlFullSelect: false,
        filtersV2: filters,
      });
      return true;
    } catch (e: any) {
      return false;
    }
  }

  static async getOne<T extends ModelObject>(
    {
      filters,
      filtersV2,
      filterDeleted = true,
      load = false,
      noCache = false,
      sqlFullSelect = false,
      dataLoader,
    }: {
      filters?: any;
      filtersV2?: Array<Filter> | Filter | FilterCombination;
      filterDeleted?: boolean;
      load?: boolean;
      noCache?: boolean;
      sqlFullSelect?: boolean;
      dataLoader?: DataLoader<any, T>;
    } = {
      load: false,
    }
  ): Promise<T> {
    const objects = await this.getAll<T>({
      filters,
      filtersV2,
      filterDeleted,
      load,
      noCache,
      sqlFullSelect,
      dataLoader,

      first: 1,
    });

    if (objects.length === 0) {
      throw new Error(
        "No data found for " + this.constructor.name + " -> " + JSON.stringify(filters)
      );
    }

    return objects[0];
  }

  static async getAll<T extends ModelObject>(
    {
      after,
      first = 10,

      filters,
      filtersV2,
      filterDeleted = true,

      load = false,
      reverseOrder = false,

      noCache: _ = false,
      sqlFullSelect = false,

      dataLoader,
    }: {
      after?: string | null;
      first?: number;
      load?: boolean;
      filters?: any;
      filtersV2?: Array<Filter> | Filter | FilterCombination;
      filterDeleted?: boolean;
      reverseOrder?: boolean;
      noCache?: boolean;
      sqlFullSelect?: boolean;
      dataLoader?: DataLoader<any, T>;
    } = {
      first: 10,
      load: false,
    }
  ): Promise<Array<T>> {
    const dbConf = this.dbConf;

    if (!dbConf.primaryKey) {
      throw new Error("Missing primary key definition");
    }

    if (sqlFullSelect) {
      return await this.getAllFullSelect({
        after,
        first,
        where: filters,
        filters: filtersV2,
        filterDeleted,
        reverseOrder,
      });
    }

    const keys = await this.getAllKeys({
      after,
      first,
      where: filters,
      filters: filtersV2,
      filterDeleted,
      reverseOrder,
    });

    return await Promise.all(
      keys
        .filter((key: any) => !!key)
        .map(async (key: any) => {
          if (dataLoader) {
            return (await dataLoader.load(key)) as T;
          }
          const obj = new this() as T;
          obj.setData({
            [dbConf.primaryKey!]: key,
          });
          if (load) {
            await obj.load();
          }
          return obj;
        })
    );
  }

  static async getCount({
    filters,
    filtersV2,
    cache_config,
  }: {
    filters?: any;
    filtersV2?: Array<Filter> | Filter | FilterCombination;
    cache_config?: {
      key: string;
      expire: number;
    };
  }) {
    const dbConf = this.dbConf;
    const table = dbConf.table;
    const primaryKey = dbConf.primaryKey;
    if (!table) {
      throw new Error("dbConf.table is not defined");
    }
    if (!primaryKey) {
      throw new Error("dbConf.primaryKey is not defined");
    }

    const count = await getCount({
      table,
      primaryKey,
      cache_config,
      where: filters
        ? dbConf.sqlMapping
          ? ObjectMapKeys(filters, dbConf.sqlMapping)
          : filters
        : undefined,
      filters: filterFiltersWithSqlMapping(filtersV2, dbConf.sqlMapping),
    });
    return count;
  }

  static async getObjectsFromKeys<T extends ModelObject>({
    keys,
    load = false,
    dataLoader,
  }: {
    load: boolean;
    keys: Array<any>;
    dataLoader?: DataLoader<any, T>;
  }): Promise<Array<T>> {
    const dbConf = this.dbConf;

    if (!dbConf.primaryKey) {
      throw new Error("Missing primary key definition");
    }

    const objects: Array<T | null> = await Promise.all(
      keys.map(async (key: any) => {
        try {
          if (dataLoader) {
            return (await dataLoader.load(key)) as T;
          }
          const obj = new this() as T;
          obj.setData({
            [dbConf.primaryKey!]: key,
          });
          if (load) {
            await obj.load();
          }
          return obj;
        } catch (e: any) {
          return null;
        }
      })
    );

    return objects.filter((obj: any) => obj !== null) as Array<T>;
  }

  static async getMultiByKeys<T extends ModelObject>(keys: Array<any>): Promise<[T | null]> {
    const { primaryKey, table } = this.dbConf;

    if (!primaryKey) {
      throw new Error("Missing primary key definition");
    }
    if (!table) {
      throw new Error("Missing table name definition");
    }

    const results = await getByMultiKeys({
      table: table,
      keyName: primaryKey,
      keys: keys.map((key) => {
        const cache_key = this.getCacheKey(key);
        return {
          keyValue: key,
          cache_key: cache_key || undefined,
        };
      }),
    });

    return (await Promise.all(
      results.map(async (resultData: any) => {
        if (!resultData || !resultData[primaryKey]) {
          return null;
        }
        try {
          return await this.getByKey<T>(resultData[primaryKey], resultData);
        } catch (e: any) {
          return null;
        }
      })
    )) as unknown as [T | null];
  }

  static getCacheKey(keyValue: string | number) {
    const dbConf = this.dbConf as DbConfType;
    if (!dbConf.cache_prefix) {
      return null;
    }
    return dbConf.cache_prefix + "_" + keyValue;
  }

  static async getByKey<T extends ModelObject>(key: any, cached_data?: any): Promise<T> {
    const dbConf = this.dbConf;
    const obj = new this() as T;

    if (!dbConf.primaryKey) {
      throw new Error("Missing primary key definition");
    }

    const primaryKeyPropertyName = this.getPropertyFromDbField(dbConf.primaryKey);
    obj.setData({
      [primaryKeyPropertyName]: key,
    });

    await obj.load(cached_data);

    return obj;
  }

  getPrimaryKeyValue() {
    const dbConf = (<any>this.constructor).dbConf;
    const currentObj = this as IIndexable;

    this.checkDbConf();

    const primaryKeyPropertyName = (<any>this.constructor).getPropertyFromDbField(
      dbConf.primaryKey
    );

    if (!primaryKeyPropertyName || !currentObj[primaryKeyPropertyName]) {
      throw new Error("Cannot load without a primary key");
    }

    const keyValue = currentObj[primaryKeyPropertyName];

    return keyValue;
  }

  async load(cached_data?: any): Promise<boolean> {
    const dbConf = (<any>this.constructor).dbConf as DbConfType;
    const currentObj = this as IIndexable;

    this.checkDbConf();
    if (!dbConf.table || !dbConf.primaryKey) {
      throw new Error("dbConf table or primaryKey missing !");
    }

    const primaryKeyPropertyName = (<any>this.constructor).getPropertyFromDbField(
      dbConf.primaryKey
    );

    if (!primaryKeyPropertyName || !currentObj[primaryKeyPropertyName]) {
      throw new Error("Cannot load without a primary key");
    }

    const keyValue = currentObj[primaryKeyPropertyName];

    const data =
      cached_data ??
      (await getByKey({
        table: dbConf.table,
        keyName: dbConf.primaryKey,
        keyValue,
        cache_key: (<any>this.constructor).getCacheKey(keyValue),
      }));
    if (!data) {
      throw new Error("No data found for " + this.constructor.name + " -> " + keyValue);
    }

    this.setDbData(data);

    return true;
  }

  async delete(): Promise<boolean> {
    const dbConf = (<any>this.constructor).dbConf;
    this.checkDbConf();

    const where = this.getDbFields({
      includeDbFields: [dbConf.primaryKey],
    });

    const result = await _delete({
      table: dbConf.table,
      where,
      cache_key: this.getThisCacheKey(),
    });

    if (result.affectedRows === 1) {
      this.deleteFromSearch();
    }

    return result.affectedRows === 1;
  }

  async create(options?: { awaitForIndexInSearch?: boolean, indexObjectInSearchEngine?: boolean }): Promise<{ insertId: number }> {
    const { awaitForIndexInSearch = false, indexObjectInSearchEngine = true } = options || {};
    const dbConf = (<any>this.constructor).dbConf;
    const dbFieldMapping = (<any>this.constructor).dbFieldMapping;
    this.checkDbConf();

    // eslint-disable-next-line
    // @ts-ignore
    this.created_at = new Date().getTime() / 1000;

    // eslint-disable-next-line
    // @ts-ignore
    this.updated_at = new Date().getTime() / 1000;

    const dbFields = this.getDbFields();

    const result = await create({
      table: dbConf.table,
      data: dbFields,
    });

    // Has a field named id
    if (dbFieldMapping.id !== undefined) {
      // eslint-disable-next-line
      // @ts-ignore
      this.id = result.insertId;
    }

    if (indexObjectInSearchEngine) {
      if (awaitForIndexInSearch) {
        await this.indexInSearch();
      } else {
        this.indexInSearch();
      }
    }

    return result;
  }

  getThisCacheKey() {
    const dbConf = (<any>this.constructor).dbConf;
    const primaryKeyPropertyName = (<any>this.constructor).getPropertyFromDbField(
      dbConf.primaryKey
    );
    const currentObj = this as IIndexable;
    if (!primaryKeyPropertyName || !currentObj[primaryKeyPropertyName]) {
      throw new Error("Cannot load without a primary key");
    }

    const keyValue = currentObj[primaryKeyPropertyName];
    return (<any>this.constructor).getCacheKey(keyValue);
  }

  async update(options?: { awaitForIndexInSearch?: boolean, indexObjectInSearchEngine?: boolean }): Promise<boolean> {
    const { awaitForIndexInSearch = false, indexObjectInSearchEngine = true } = options || {};
    const dbConf = (<any>this.constructor).dbConf;
    this.checkDbConf();

    // eslint-disable-next-line
    // @ts-ignore
    this.updated_at = new Date().getTime() / 1000;

    const data = this.getDbFields({
      excludeDbFields: [dbConf.primaryKey],
    });
    const where = this.getDbFields({
      includeDbFields: [dbConf.primaryKey],
    });

    const result = await update({
      table: dbConf.table,
      where,
      data,
      cache_key: this.getThisCacheKey(),
    });

    if (indexObjectInSearchEngine) {
      if (awaitForIndexInSearch) {
        await this.indexInSearch();
      } else {
        this.indexInSearch();
      }
    }

    return !!result.changedRows;
  }

  async deleteFromSearch(): Promise<boolean> {
    if (!getCoreConfig().indexObjectInSearchEngine) {
      return false;
    }

    const dbConf = (<any>this.constructor).dbConf;
    this.checkDbConf();

    if (!dbConf.searchIndex) {
      return false;
    }

    const keyValue = this.getPrimaryKeyValue();

    const esClient = getEsClient();
    await esClient.delete({
      id: keyValue,
      index: dbConf.searchIndex,
    });

    return true;
  }

  async indexInSearch(): Promise<boolean> {
    if (!getCoreConfig().indexObjectInSearchEngine) {
      return false;
    }

    const dbConf = (<any>this.constructor).dbConf;
    this.checkDbConf();

    if (!dbConf.searchIndex) {
      return false;
    }

    const keyValue = this.getPrimaryKeyValue();
    await this.load();
    const data = await this.exportForEs();
    const esClient = getEsClient();
    await esClient.index({
      id: keyValue,
      index: dbConf.searchIndex,
      body: data,
    });

    return true;
  }
}
