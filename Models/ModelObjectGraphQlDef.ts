import "reflect-metadata";

type FieldConf = {
  type?: any;
  required?: boolean;
  directives?: Array<any>;
  arguments?: { [key: string]: any };
};

type ClassConf = {
  type?: "object" | "input";
  defineList?: boolean;
  fields?: {
    [key: string]: FieldConf;
  };
};

const gqlTypes: {
  [key: string]: ClassConf;
} = {};

export function gql(
  _: TemplateStringsArray,
  __: {
    create: boolean;
    update: boolean;
  } = { create: false, update: false }
) {
  // eslint-disable-next-line
  return () => {};
}

export class Int {}
export class Float {}
export class ID {}

export const InputType = (name?: string) => (target: any) => {
  const className = name || target.name;

  if (!gqlTypes[className]) {
    gqlTypes[className] = {};
  }

  gqlTypes[className].type = "input";
};

export const ObjectType = () => (target: any) => {
  const className = target.name;

  if (!gqlTypes[className]) {
    gqlTypes[className] = {};
  }

  gqlTypes[className].type = "object";
};

export const GQLList = () => (target: any) => {
  const className = target.name;

  if (!gqlTypes[className]) {
    gqlTypes[className] = {};
  }

  gqlTypes[className].defineList = true;
};

const createFieldOrDirective = (target: any, propertyKey: string) => {
  const className = target.constructor.name;

  if (!gqlTypes[className]) {
    gqlTypes[className] = {};
  }

  if (!gqlTypes[className].fields) {
    gqlTypes[className].fields = {};
  }

  if (!gqlTypes[className]!.fields![propertyKey]) {
    gqlTypes[className]!.fields![propertyKey] = {
      directives: [],
      arguments: {},
    };
  }

  return gqlTypes[className].fields![propertyKey];
};

export const GQLFieldArguments =
  (args: { [key: string]: any }) => (target: any, propertyKey: string) => {
    const propertyConfig = createFieldOrDirective(target, propertyKey);
    propertyConfig.arguments = {
      ...propertyConfig.arguments,
      ...args,
    };
  };

export const GQLFieldType =
  (type: any, options?: { required?: boolean; debug?: boolean }) =>
  (target: any, propertyKey: string) => {
    const propertyConfig = createFieldOrDirective(target, propertyKey);
    propertyConfig!.type = type;
    propertyConfig!.required = options?.required || false;
  };
export const GQLFieldDirective = (directive: any) => (target: any, propertyKey: string) => {
  const propertyConfig = createFieldOrDirective(target, propertyKey);
  propertyConfig!.directives!.push(directive);
};

const getGraphQlFieldType = (type: any) => {
  switch (true) {
    case typeof type === "function" && type.constructor && typeof type.constructor === "function":
      return type.name;
    case typeof type === "string":
      return type;
    default:
      return "";
  }
};

const getGraphQlFieldArguments = (args: { [key: string]: any } | undefined) => {
  if (!args) {
    return "";
  }
  const argsInString: string = Object.keys(args)
    .map((key: string) => {
      return `${key}: ${getGraphQlFieldType(args[key])}`;
    })
    .join(", ");

  if (argsInString === "") {
    return argsInString;
  }

  return `(${argsInString})`;
};

const getGraphQlFieldDirectives = (directives: Array<string>) => {
  return directives.join(" ");
};

// email: String @auth(requires: ADMIN),
const getGraphQlFieldDef = (fieldName: string, fieldConf: FieldConf) =>
  [
    `${fieldName}${getGraphQlFieldArguments(fieldConf.arguments)}: ${getGraphQlFieldType(
      fieldConf.type
    )}${fieldConf.required ? "!" : ""}`,
    getGraphQlFieldDirectives(fieldConf.directives || []),
  ].join(" ");

const getGraphQlTypeOrInput = (className: string): string => {
  if (!gqlTypes[className].type) {
    return "";
  }

  const fieldKeys = Object.keys(gqlTypes[className].fields || {});

  if (fieldKeys.length === 0) {
    return "";
  }

  return `
${gqlTypes[className].type === "object" ? "type" : "input"} ${className} {
    ${fieldKeys.map((fieldName) =>
      getGraphQlFieldDef(fieldName, gqlTypes[className]!.fields![fieldName])
    ).join(`,
    `)}
}
`;
};

const getGraphQlTypeList = (className: string): string => {
  if (gqlTypes[className].type !== "object") {
    return "";
  }

  if (!gqlTypes[className].defineList) {
    return "";
  }

  return `
type ${className}Edge {
    cursor: String
    node: ${className}
}
type ${className}List {
    edges: [${className}Edge]
    pageInfo: PageInfo
}
`;
};

// ${getGraphQlTypeOrInput(className, gqlTypes[className])}

export const getGraphQlTypesAndInputs = () =>
  Object.keys(gqlTypes)
    .map((className) => {
      return [getGraphQlTypeOrInput(className), getGraphQlTypeList(className)].join("\n");
    })
    .join("\n");

const otherGqlDefs: Array<any> = [];
export const registerOtherGqlDefs = (gqlDef: any) => otherGqlDefs.push(gqlDef);
export const getOtherGqlDefs = () => otherGqlDefs;
