import Filter from "../Services/Sql/Filter";
import FilterCombination from "../Services/Sql/FilterCombination";

export const filterFiltersWithSqlMapping = (
  filters?: Array<Filter> | Filter | FilterCombination,
  sqlMapping?: { [key: string]: any }
): Array<Filter> | Filter | FilterCombination => {
  if (!filters || !sqlMapping) {
    return [];
  }

  if (filters instanceof Filter) {
    return filterFilterWithSqlMapping(filters, sqlMapping) || [];
  }

  if (filters instanceof FilterCombination) {
    return filterFilterCombinationWithSqlMapping(filters, sqlMapping) || [];
  }

  return filterArrayFiltersWithSqlMapping(filters, sqlMapping);
};

const filterFilterCombinationWithSqlMapping = (
  filterCombination: FilterCombination,
  sqlMapping: { [key: string]: any }
): FilterCombination | null => {
  const filters = filterCombination
    .getFilters()
    .map((filter: Filter | FilterCombination) => {
      if (filter instanceof Filter) {
        return filterFilterWithSqlMapping(filter, sqlMapping);
      } else {
        return filterFilterCombinationWithSqlMapping(filter, sqlMapping);
      }
    })
    .filter((filter: Filter | FilterCombination | null) => filter !== null) as Array<
    Filter | FilterCombination
  >;
  if (filters.length === 0) {
    return null;
  }
  filterCombination.setFilters(filters);
  return filterCombination;
};

const filterFilterWithSqlMapping = (
  filter: Filter,
  sqlMapping: { [key: string]: any }
): Filter | null => {
  const fieldName = filter.getFieldName();
  if (!sqlMapping[fieldName]) {
    return null;
  }
  filter.setFieldName(sqlMapping[fieldName]);
  return filter;
};

const filterArrayFiltersWithSqlMapping = (
  filters: Array<Filter>,
  sqlMapping: { [key: string]: any }
): Array<Filter> => {
  return filters
    .map((filter: Filter) => {
      return filterFilterWithSqlMapping(filter, sqlMapping);
    })
    .filter((filter: Filter | null) => {
      return filter !== null;
    }) as Array<Filter>;
};
